# Manually

## Local

```
docker build -t <img_name>:<tag> .
docker run --name <any_name> -p <port>:5002 <img_name>:<tag>
```

You can then access the API UI at http:\/\/localhost:\<port\>

## Kube

Adapt the image and app names to yours.

```
kubectl apply -f deployment.yml
```

The UI will be accessible at your cluster's public ip, port 80.

## What it looks like

![](screen.png)
