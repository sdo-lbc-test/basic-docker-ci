FROM python:3.9.13

WORKDIR /usr/local/bin

COPY ./src ./src

RUN pip install --upgrade pip

RUN pip install -r ./src/requirements.txt

CMD ["python", "./src/app.py"]
