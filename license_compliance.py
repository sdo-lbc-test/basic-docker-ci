"""
Ensures that 'pip-licenses' read licenses are non-commercial
"""
import sys
import json
import warnings
import pandas as pd
warnings.filterwarnings('ignore')


if __name__ == '__main__':
    # Read 'pip-licenses' output
    df = pd.DataFrame(json.loads("\n".join(sys.stdin.readlines())))
    df['License'] = df['License'].apply(lambda x: str(x).lower())
    # Filter the non usual licenses
    attention_df = df[~df['License'].str
                      .contains("(mit|bsd|public|gnu|apache"
                                "|free|python|isc|cc0|unknown)")]
    if len(attention_df) == 0:
        print(df['License'].value_counts())
        print("No commercial license detected !")
    else:
        print(attention_df.reset_index(drop=True))
        raise Exception(f"/!\\ {len(attention_df)} package(s) with potentially"
                        f" commercial licenses detected /!\\")
