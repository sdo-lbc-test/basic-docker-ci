from src.app import app
import pytest


@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_one(client):
    res = client.get('/one')
    assert res.get_json() == {"myfield": "92:48"}
