"""
Simple API
"""
from flask import Flask
from flask_restx import Api, Resource


app = Flask(__name__)
api = Api(app=app,
          version='92.48',
          title='test_api',
          description='description TBD')


ns = api.namespace('one', description='namespace one')


@ns.route('')
class ClassOne(Resource):
    def get(self):
        return {"myfield": "92:48"}

    def post(self):
        return {"status": "received"}


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5002)
